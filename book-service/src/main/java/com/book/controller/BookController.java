package com.book.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.book.dto.SearchDto;
import com.book.entity.Book;
import com.book.service.BookService;

@RestController
public class BookController {
	
	@Autowired
	BookService bookService;
	
	@PostMapping("/v1/add/book")
	public ResponseEntity<Book> insertBook(@RequestBody Book book){
		//Async call for Events
		bookService.createEvent("insertBook");
		return new ResponseEntity<Book>(bookService.insertBook(book),HttpStatus.CREATED);
	}
	
	@DeleteMapping("/v1/remove/book")
	public ResponseEntity<Book> removeBook(@RequestBody Book book){
		//Async call for Events
		bookService.createEvent("removeBook");
		return new ResponseEntity<Book>(bookService.removeBook(book),HttpStatus.OK);
	}
	
	@PutMapping("/v1/update/book")
	public ResponseEntity<Book> updateBook(@RequestBody Book book){
		//Async call for Events
		bookService.createEvent("updateBook");
		return new ResponseEntity<Book>(bookService.updateBook(book),HttpStatus.OK);
	}
	
	@PostMapping("/v1/search")
    public ResponseEntity<List<Book>> searchForBook(@RequestBody SearchDto searchDto) {
        return new ResponseEntity<>(bookService.searchForBook(searchDto), HttpStatus.OK);
    }

}

package com.book.dto;

import lombok.Data;

@Data
public class SearchDto {

	private String fieldName;
	private String searchKey;
}

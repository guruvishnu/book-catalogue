package com.book.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.book.entity.Book;

@Repository
public interface BookRepository extends CrudRepository<Book, Long>{

	@Query(value="select * from book.book b  where b.author like  %:searchKey% ;", nativeQuery = true)
	List<Book> searchByAuthor(@Param("searchKey") String searchKey);
	
	@Query(value="select * from book.book b  where b.title like  %:searchKey% ;", nativeQuery = true)
	List<Book> searchByTitle(@Param("searchKey") String searchKey);
	
	@Query(value="select * from book.book b  where b.isbn_number like  %:searchKey% ;", nativeQuery = true)
	List<Book> searchByIsbn(@Param("searchKey") String searchKey);
}

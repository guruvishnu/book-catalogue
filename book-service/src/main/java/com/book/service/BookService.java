package com.book.service;

import java.util.List;

import com.book.dto.SearchDto;
import com.book.entity.Book;

public interface BookService {

	Book insertBook(Book book);

	Book removeBook(Book book);

	Book updateBook(Book book);

	void createEvent(String string);

	List<Book> searchForBook(SearchDto searchDto);

}

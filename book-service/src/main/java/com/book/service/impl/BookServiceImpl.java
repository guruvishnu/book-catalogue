package com.book.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.book.dto.SearchDto;
import com.book.entity.Book;
import com.book.repository.BookRepository;
import com.book.service.BookService;

@Service
public class BookServiceImpl implements BookService {

	Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

	@Autowired
	BookRepository bookRepository;

	@Override
	public Book insertBook(Book book) {
		logger.info("insertBook () {}", book);
		return bookRepository.save(book);
	}

	@Override
	public Book removeBook(Book book) {
		logger.info("removeBook () {}", book);
		bookRepository.delete(book);
		return book;
	}

	@Override
	public Book updateBook(Book book) {
		logger.info("updateBook () {}", book);
		bookRepository.findById(book.getId()).orElseThrow(null);
		return bookRepository.save(book);
	}

	@Override
	@Async
	public void createEvent(String event) {
		logger.info("createEvent for {}", event);

	}

	@Override
	public List<Book> searchForBook(SearchDto searchDto) {
		logger.info("searchForBook {}", searchDto);

		List<Book> books = new ArrayList<>();

		if (searchDto.getFieldName().equalsIgnoreCase("title"))
			books = bookRepository.searchByTitle(searchDto.getSearchKey());
		else if (searchDto.getFieldName().equalsIgnoreCase("author"))
			books = bookRepository.searchByAuthor(searchDto.getSearchKey());
		else if (searchDto.getFieldName().equalsIgnoreCase("isbn"))
			books = bookRepository.searchByIsbn(searchDto.getSearchKey());

		return books;
	}

}
